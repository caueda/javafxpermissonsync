/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao;

import br.gov.mt.mti.permissionsync.dao.interfaces.Importable;
import br.gov.mt.mti.permissionsync.dominio.Ambiente;
import br.gov.mt.mti.permissionsync.vo.BaseModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author c8757550
 */
public class SincronizarDao extends AbstractDao {    
    public static final String TB_PERMISSAO="BCNTB0612";
    public static final String TB_FUNCIONALIDADE="BCNTB0613";
    public static final String TB_FUNCIONALIDADE_PERMISSAO="BCNTB0615";
    public static final String TB_PERFIL_FUNCIONALIDADE="BCNTB0614";    
    /*
    public static final String TB_PERMISSAO="PERMISSAO";
    public static final String TB_FUNCIONALIDADE="FUNCIONALIDADE";
    public static final String TB_FUNCIONALIDADE_PERMISSAO="FUNCIONALIDADE_PERMISSAO";
    public static final String TB_PERFIL_FUNCIONALIDADE="PERFIL_FUNCIONALIDADE";
    */
    public SincronizarDao() throws Exception {
        super();
    }
    
    Importable sincronizarPermissao = (connOrigem, connDestino) -> {
        int inserted = 0;
        int error = 0;
        Set<Long> idsFromDestino = new HashSet<>();
        Set<Long> idsFromOrigem = new HashSet<>();
        
        String sqlPermissaoId = "SELECT PERMISSAO_ID FROM TABELA ORDER BY PERMISSAO_ID";
        
        try (Statement stmt = connDestino.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA", TB_PERMISSAO));
            idsFromDestino.clear();
            while(rs.next()){
                idsFromDestino.add(rs.getLong("PERMISSAO_ID"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        try (Statement stmt = connOrigem.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA", "PERMISSAO"));
            idsFromOrigem.clear();
            while(rs.next()){
                idsFromOrigem.add(rs.getLong("PERMISSAO_ID"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        HashSet<Long> remover = new HashSet<>();
        remover.addAll(idsFromDestino);
        remover.removeAll(idsFromOrigem);        
        
        try (Statement stmt = connDestino.createStatement()){             
            for(Long id : remover){
                //Deleta da tabela de relacionamento de funcionalidade/permissão
                stmt.executeUpdate("DELETE FROM " + TB_FUNCIONALIDADE_PERMISSAO + " WHERE PERMISSAO_ID = " + id.toString());
                //Deleta da tabela de permissão
                stmt.executeUpdate("DELETE FROM " + TB_PERMISSAO + " WHERE PERMISSAO_ID = " + id.toString());
                connDestino.commit();
            }                     
        } catch(SQLException e){
            e.printStackTrace();
        }    
        HashSet<Long> incluir = new HashSet<>();
        incluir.addAll(idsFromOrigem);
        incluir.removeAll(idsFromDestino);
        
               
        String insertSQL = "INSERT INTO " + TB_PERMISSAO + "(PERMISSAO_ID,TIPO_PERMISSAO,NOME,DESCRICAO,ACTION,DATA_CRIACAO,DATA_ATUALIZACAO,VERSAO)"
                + "VALUES(?,?,?,?,?,?,?,?)";
        String sqlFromOrigem = "SELECT PERMISSAO_ID,NOME,ACTION,DESCRICAO,TIPO_PERMISSAO,DATA_CRIACAO,DATA_ATUALIZACAO,VERSAO FROM PERMISSAO WHERE PERMISSAO_ID=?";
        try(PreparedStatement insert = connDestino.prepareStatement(insertSQL);
            PreparedStatement consultar = connOrigem.prepareStatement(sqlFromOrigem)){
            for(Long id : incluir){
                try {
                    consultar.setLong(1, id);
                    ResultSet rs = consultar.executeQuery();
                    while(rs.next()){
                        insert.setLong(1,rs.getLong("PERMISSAO_ID"));
                        insert.setInt(2, rs.getInt("TIPO_PERMISSAO"));
                        insert.setString(3, rs.getString("NOME"));
                        insert.setString(4, rs.getString("DESCRICAO"));
                        insert.setString(5, rs.getString("ACTION"));
                        insert.setDate(6, rs.getDate("DATA_CRIACAO"));
                        if(rs.getDate("DATA_ATUALIZACAO") != null){
                            insert.setDate(7, rs.getDate("DATA_ATUALIZACAO"));
                        } else {
                            insert.setNull(7, Types.NULL);
                        }
                        insert.setInt(8, rs.getInt("VERSAO"));
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        connDestino.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    };
    
    Importable sincronizarFuncionalidade = (connOrigem, connDestino) -> {
        int inserted = 0;
        int error = 0;
        Set<Long> idsFromDestino = new HashSet<>();
        Set<Long> idsFromOrigem = new HashSet<>();
        
        String sqlPermissaoId = "SELECT FUNCIONALIDADE_ID FROM TABELA ORDER BY FUNCIONALIDADE_ID";
        
        try (Statement stmt = connDestino.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA",TB_FUNCIONALIDADE));
            idsFromDestino.clear();
            while(rs.next()){
                idsFromDestino.add(rs.getLong("FUNCIONALIDADE_ID"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        try (Statement stmt = connOrigem.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA","FUNCIONALIDADE"));
            idsFromOrigem.clear();
            while(rs.next()){
                idsFromOrigem.add(rs.getLong("FUNCIONALIDADE_ID"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        HashSet<Long> remover = new HashSet<>();
        remover.addAll(idsFromDestino);
        remover.removeAll(idsFromOrigem);        
        
        try (Statement stmt = connDestino.createStatement()){             
            for(Long id : remover){
                //Deleta da tabela de relacionamento de Perfil/Funcionalidade
                stmt.executeUpdate("DELETE FROM " + TB_PERFIL_FUNCIONALIDADE + " WHERE FUNCIONALIDADE_ID = " + id.toString());
                //Deleta da tabela de Funcionalidade
                stmt.executeUpdate("DELETE FROM " + TB_FUNCIONALIDADE + " WHERE FUNCIONALIDADE_ID = " + id.toString());
                connDestino.commit();
            }      
        } catch(SQLException e){
            e.printStackTrace();
        }    
        HashSet<Long> incluir = new HashSet<>();
        incluir.addAll(idsFromOrigem);
        incluir.removeAll(idsFromDestino);
        
               
        String insertSQL = "INSERT INTO " + TB_FUNCIONALIDADE + "(FUNCIONALIDADE_ID, NOME, DESCRICAO, TIPO_SUBSISTEMA, USR_ATRIBUICAO_FUNC, DATA_CRIACAO, DATA_ATUALIZACAO,"
                    + "USR_SITUACAO, VERSAO)"
                    + "VALUES(?,?,?,?,?,?,?,?,?)";
        String sqlFromOrigem = "SELECT FUNCIONALIDADE_ID, NOME, DESCRICAO,\n" +
                                "       TIPO_SUBSISTEMA, USR_ATRIBUICAO_FUNC,\n" +
                                "       DATA_CRIACAO, DATA_ATUALIZACAO,\n" +
                                "       USR_SITUACAO,VERSAO\n" +
                                "  FROM FUNCIONALIDADE\n" +
                                " WHERE FUNCIONALIDADE_ID = ?";
        try(PreparedStatement insert = connDestino.prepareStatement(insertSQL);
            PreparedStatement consultar = connOrigem.prepareStatement(sqlFromOrigem)){
            for(Long id : incluir){
                try {
                    consultar.setLong(1, id);
                    ResultSet rs = consultar.executeQuery();
                    while(rs.next()){
                        insert.setLong(1,rs.getLong("FUNCIONALIDADE_ID"));
                        insert.setString(2, rs.getString("NOME"));
                        insert.setString(3, rs.getString("DESCRICAO"));
                        if(rs.getInt("TIPO_SUBSISTEMA") != 0){
                            insert.setInt(4, rs.getInt("TIPO_SUBSISTEMA"));
                        } else {
                            insert.setNull(4, Types.NULL);
                        }
                        
                        if(rs.getLong("USR_ATRIBUICAO_FUNC") != 0){                            
                            insert.setLong(5, rs.getLong("USR_ATRIBUICAO_FUNC"));
                        } else {
                            insert.setNull(5, Types.NULL);
                        }
                        insert.setDate(6, rs.getDate("DATA_CRIACAO"));
                        if(rs.getDate("DATA_ATUALIZACAO") != null){
                            insert.setDate(7, rs.getDate("DATA_ATUALIZACAO"));
                        } else {
                            insert.setNull(7, Types.NULL);
                        }
                        insert.setInt(8, rs.getInt("USR_SITUACAO"));
                        insert.setInt(9, rs.getInt("VERSAO"));
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        connDestino.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        
    };
    
    Importable sincronizarFuncionalidadePermissao = (connOrigem, connDestino) -> {
        int inserted = 0;
        int error = 0;
        Set<String> idsFromDestino = new HashSet<>();
        Set<String> idsFromOrigem = new HashSet<>();
        
        String sqlPermissaoId = "SELECT FUNCIONALIDADE_ID||'#'||PERMISSAO_ID AS IDEN FROM TABELA ORDER BY FUNCIONALIDADE_ID, PERMISSAO_ID";
        
        try (Statement stmt = connDestino.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA",TB_FUNCIONALIDADE_PERMISSAO));
            idsFromDestino.clear();
            while(rs.next()){
                idsFromDestino.add(rs.getString("IDEN"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        try (Statement stmt = connOrigem.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA","FUNC_PERM"));
            idsFromOrigem.clear();
            while(rs.next()){
                idsFromOrigem.add(rs.getString("IDEN"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        HashSet<String> remover = new HashSet<>();
        remover.addAll(idsFromDestino);
        remover.removeAll(idsFromOrigem);        
        
        try (Statement stmt = connDestino.createStatement()){             
            for(String id : remover){
                String[] ids = id.split("#");
                //Deleta da tabela de relacionamento de funcionalidade/permissão
                stmt.executeUpdate("DELETE FROM " + TB_FUNCIONALIDADE_PERMISSAO + " WHERE FUNCIONALIDADE_ID = " + ids[0] + " AND PERMISSAO_ID = " + ids[1]);                
                connDestino.commit();
            }         
        } catch(SQLException e){
            e.printStackTrace();
        }    
        HashSet<String> incluir = new HashSet<>();
        incluir.addAll(idsFromOrigem);
        incluir.removeAll(idsFromDestino);
        
               
        String insertSQL = "INSERT INTO " + TB_FUNCIONALIDADE_PERMISSAO + "(FUNCIONALIDADE_ID, PERMISSAO_ID)"
                + "VALUES(?,?)";
        String sqlFromOrigem = "SELECT FUNCIONALIDADE_ID, PERMISSAO_ID FROM FUNC_PERM WHERE FUNCIONALIDADE_ID=? AND PERMISSAO_ID=?";
        try(PreparedStatement insert = connDestino.prepareStatement(insertSQL);
            PreparedStatement consultar = connOrigem.prepareStatement(sqlFromOrigem)){
            for(String id : incluir){
                try {
                    String[] ids = id.split("#");
                    consultar.setLong(1, Long.valueOf(ids[0]));
                    consultar.setLong(2, Long.valueOf(ids[1]));
                    ResultSet rs = consultar.executeQuery();
                    while(rs.next()){
                        insert.setLong(1,rs.getLong("FUNCIONALIDADE_ID"));
                        insert.setInt(2, rs.getInt("PERMISSAO_ID"));                        
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        connDestino.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    };
    
    Importable sincronizarPerfilFuncionalidade = (connOrigem, connDestino) -> {
        int inserted = 0;
        int error = 0;
        Set<String> idsFromDestino = new HashSet<>();
        Set<String> idsFromOrigem = new HashSet<>();
        
        String sqlPermissaoId = "SELECT PERFIL_ID||'#'||FUNCIONALIDADE_ID AS IDEN FROM TABELA WHERE PERFIL_ID = 1 ORDER BY PERFIL_ID,FUNCIONALIDADE_ID";
        
        try (Statement stmt = connDestino.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA",TB_PERFIL_FUNCIONALIDADE));
            idsFromDestino.clear();
            while(rs.next()){
                idsFromDestino.add(rs.getString("IDEN"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        try (Statement stmt = connOrigem.createStatement()){ 
            ResultSet rs = stmt.executeQuery(sqlPermissaoId.replace("TABELA","PERFIL_FUNCIONALIDADE"));
            idsFromOrigem.clear();
            while(rs.next()){
                idsFromOrigem.add(rs.getString("IDEN"));
            }            
        } catch(SQLException e){
            e.printStackTrace();
        }
        
        HashSet<String> remover = new HashSet<>();
        remover.addAll(idsFromDestino);
        remover.removeAll(idsFromOrigem);        
        
        try (Statement stmt = connDestino.createStatement()){             
            for(String id : remover){
                String[] ids = id.split("#");
                //Deleta da tabela de relacionamento de funcionalidade/permissão
                stmt.executeUpdate("DELETE FROM " + TB_PERFIL_FUNCIONALIDADE + " WHERE PERFIL_ID = " + ids[0] + " AND FUNCIONALIDADE_ID = " + ids[1]);                
                connDestino.commit();
            }         
        } catch(SQLException e){
            e.printStackTrace();
        }    
        HashSet<String> incluir = new HashSet<>();
        incluir.addAll(idsFromOrigem);
        incluir.removeAll(idsFromDestino);
        
               
        String insertSQL = "INSERT INTO " + TB_PERFIL_FUNCIONALIDADE + "(PERFIL_ID, FUNCIONALIDADE_ID)"
                + "VALUES(?,?)";
        String sqlFromOrigem = "SELECT PERFIL_ID, FUNCIONALIDADE_ID FROM PERFIL_FUNCIONALIDADE WHERE PERFIL_ID=1 AND FUNCIONALIDADE_ID=?";
        try(PreparedStatement insert = connDestino.prepareStatement(insertSQL);
            PreparedStatement consultar = connOrigem.prepareStatement(sqlFromOrigem)){
            for(String id : incluir){
                try {
                    String[] ids = id.split("#");                    
                    consultar.setLong(1, Long.valueOf(ids[1]));
                    ResultSet rs = consultar.executeQuery();
                    while(rs.next()){
                        insert.setLong(1,rs.getLong("PERFIL_ID"));
                        insert.setInt(2, rs.getInt("FUNCIONALIDADE_ID"));                        
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        connDestino.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    };
    

    @Override
    public BaseModel getFromResultSet(ResultSet rs) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTableName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getIdColumn() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void executar(Ambiente ambiente) throws Exception {
        Connection connOrigem = null;
        Connection connDestino = null;
        try {       
            connOrigem = getMsAccessConnection(); 
            switch(ambiente){
                case INTEGRACAO:
                    connDestino = getIntegraConnection();
                    break;
                case HOMOLOGACAO:
                    connDestino = getHomologaConnection();
                    break;
                default: connDestino = getProducaoConnection();
            }
            sincronizarPermissao.execute(connOrigem, connDestino);
            sincronizarFuncionalidade.execute(connOrigem, connDestino);
            sincronizarFuncionalidadePermissao.execute(connOrigem, connDestino);
            sincronizarPerfilFuncionalidade.execute(connOrigem, connDestino);
            
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            close(connOrigem, connDestino);
        }
    }
}

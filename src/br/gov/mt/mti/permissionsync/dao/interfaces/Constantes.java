/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao.interfaces;

/**
 *
 * @author c8757550
 */
public interface Constantes {
    public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final String ACCESS_DRIVER = "net.ucanaccess.jdbc.UcanaccessDriver";
    
    public static final String ACCESS_DB = "access_db";
    
    public static final String DB_INTEGRA_HOST = "db_integra.host";
    public static final String DB_INTEGRA_PORT = "db_integra.port";
    public static final String DB_INTEGRA_SID = "db_integra.sid";
    public static final String DB_INTEGRA_USER = "db_integra.user";
    public static final String DB_INTEGRA_PASSWORD = "db_integra.password";
    
    public static final String DB_HOMOLOGA_HOST = "db_homologa.host";
    public static final String DB_HOMOLOGA_PORT = "db_homologa.port";
    public static final String DB_HOMOLOGA_SID = "db_homologa.sid";
    public static final String DB_HOMOLOGA_USER = "db_homologa.user";
    public static final String DB_HOMOLOGA_PASSWORD = "db_homologa.password";
    
    public static final String DB_PRODUCAO_HOST = "db_producao.host";
    public static final String DB_PRODUCAO_PORT = "db_producao.port";
    public static final String DB_PRODUCAO_SID = "db_producao.sid";
    public static final String DB_PRODUCAO_USER = "db_producao.user";
    public static final String DB_PRODUCAO_PASSWORD = "db_producao.password";
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao.interfaces;

import java.sql.Connection;

/**
 *
 * @author c8757550
 */
public interface Importable {
    void execute(final Connection conn1, final Connection conn2);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao;

import br.gov.mt.mti.permissionsync.dao.interfaces.Constantes;
import br.gov.mt.mti.permissionsync.dominio.Situacao;
import br.gov.mt.mti.permissionsync.dominio.Subsistema;
import br.gov.mt.mti.permissionsync.dominio.TipoPermissao;
import br.gov.mt.mti.permissionsync.util.PropertyUtil;
import br.gov.mt.mti.permissionsync.util.Util;
import br.gov.mt.mti.permissionsync.vo.BaseModel;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author c8757550
 */
public abstract class AbstractDao<T extends BaseModel> implements Serializable, Constantes {
    
    protected Connection connection;
    
    public static final String DATA_CRIACAO = "DATA_CRIACAO";
    public static final String DATA_ATUALIZACAO = "DATA_ATUALIZACAO";
    public static final String VERSAO = "VERSAO";
    
    public abstract T getFromResultSet(final ResultSet rs) throws SQLException;
    
    public abstract String getTableName();
    
    public abstract String getIdColumn();
    
    public Long getMaxID(){
        Statement statement = null;
        ResultSet resultSet = null;
        Long maxID = null;
        try {
            statement = connection.createStatement();

            // Step 2.C: Executing SQL &amp; retrieve data into ResultSet
            resultSet = statement.executeQuery("SELECT MAX(" + getIdColumn() + ") " + getIdColumn() + " FROM " + getTableName());

            // processing returned data and printing into console
            while(resultSet.next()) {
                maxID = resultSet.getLong(getIdColumn());
            }
        } catch(Exception e){
        } finally {
            close(statement);
            close(resultSet);
        }
        return maxID;
    }
    
    public String getSQLSelect(String ... columns){
        String sql = "SELECT :COLUMNS FROM " + getTableName() + " WHERE 1=1 \n";        
        StringBuilder sqlColunas = new StringBuilder("");
        for(int i=0; i<columns.length; i++){
            String column = columns[i];
            sqlColunas.append(column).append((i==(columns.length-1))? "" : ",");
        }        
        return sql.replace(":COLUMNS",sqlColunas.toString());
    }
    
    public String getSQLSelectTOPN(Integer top, String ... columns){
        String sql = "SELECT TOP :N :COLUMNS FROM " + getTableName() + " WHERE 1=1 \n";        
        StringBuilder sqlColunas = new StringBuilder("");
        for(int i=0; i<columns.length; i++){
            String column = columns[i];
            sqlColunas.append(column).append((i==(columns.length-1))? "" : ",");
        }        
        return sql.replace(":COLUMNS",sqlColunas.toString()).replace(":N", top.toString());
    }
    
    public String getSQLInsert(String ... columns){
        StringBuilder sql = new StringBuilder("INSERT INTO " + getTableName() + "(");        
        StringBuilder sqlParam = new StringBuilder("(");
        for(int i=0; i<columns.length; i++){
            String column = columns[i];
            sql.append(column).append((i==(columns.length-1))? "" : ",");
            sqlParam.append("?").append((i==(columns.length-1))? "" : ",");
        }
        sql.append(") VALUES ");
        sqlParam.append(")\n");
        sql.append(sqlParam.toString());
        
        return sql.toString();
    }
    
    public String getSQLUpdate(Long id, String ... columns){
        StringBuilder sql = new StringBuilder("UPDATE " + getTableName() + " SET ");
        for(int i=0; i<columns.length; i++){
            String column = columns[i];
            sql.append(column).append("=? ").append((i==(columns.length-1)) ? "" : ",");            
        }        
        sql.append(" WHERE ")
           .append(getIdColumn())
           .append("=")
           .append(id.toString());
        
        return sql.toString();
    }
    
    public java.sql.Date getDataAtual(){
        return new java.sql.Date(new java.util.Date().getTime());
    }
    
    public AbstractDao() throws Exception {
        super();
        this.connection = getMsAccessConnection();
    }
    
    public static Connection getMsAccessConnection() throws Exception {
        Class.forName(ACCESS_DRIVER);        
        String msAccDB = PropertyUtil.getProperty(Constantes.ACCESS_DB);
        String dbURL = "jdbc:ucanaccess://" + msAccDB;
        return DriverManager.getConnection(dbURL);
    }
    
    public final Situacao getSituacaoFromResultSet(final ResultSet rs, String column) throws SQLException {
        Situacao situacaoValue = Situacao.get(rs.getInt(column));    
        return situacaoValue;
    }
    
    public final TipoPermissao getTipoPermissaoFromResultSet(final ResultSet rs, String column) throws SQLException {
        TipoPermissao situacaoValue = TipoPermissao.get(rs.getInt(column));    
        return situacaoValue;
    }
    
    public final Subsistema getSubsistemaFromResultSet(final ResultSet rs, String column) throws SQLException {
        Subsistema subsistemaValue = Subsistema.get(rs.getInt(column));    
        return subsistemaValue;
    }
    
    public AbstractDao(Connection conn){
        this.connection = conn;
    }
     
    public void close(AutoCloseable closeable){
        try {
            if(closeable != null){
                closeable.close();
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void close(AutoCloseable ... closeables){
        for(AutoCloseable c : closeables) close(c);    
    }
    
    public void close(){
        try {
            if(connection != null) connection.close();
        } catch(Exception e){}        
    }
    
    public static Connection getIntegraConnection() throws Exception {
        Class.forName(ORACLE_DRIVER);
        String integraHost = PropertyUtil.getProperty(DB_INTEGRA_HOST);
        String integraPort = PropertyUtil.getProperty(DB_INTEGRA_PORT);
        String integraSid = PropertyUtil.getProperty(DB_INTEGRA_SID);
        String integraUser = PropertyUtil.getProperty(DB_INTEGRA_USER);
        String integraPasswd = PropertyUtil.getProperty(DB_INTEGRA_PASSWORD);
        
        Connection integraConn = 
                DriverManager.getConnection("jdbc:oracle:thin:@" + integraHost + ":"+integraPort + ":" + integraSid,Util.decripta(integraUser), Util.decripta(integraPasswd));
        
        return integraConn;
    }
    
        public static Connection getHomologaConnection() throws Exception {
        Class.forName(ORACLE_DRIVER);
        String integraHost = PropertyUtil.getProperty(DB_HOMOLOGA_HOST);
        String integraPort = PropertyUtil.getProperty(DB_HOMOLOGA_PORT);
        String integraSid = PropertyUtil.getProperty(DB_HOMOLOGA_SID);
        String integraUser = PropertyUtil.getProperty(DB_HOMOLOGA_USER);
        String integraPasswd = PropertyUtil.getProperty(DB_HOMOLOGA_PASSWORD);
        
        Connection integraConn = 
                DriverManager.getConnection("jdbc:oracle:thin:@" + integraHost + ":"+integraPort + ":" + integraSid,Util.decripta(integraUser), Util.decripta(integraPasswd));
        
        return integraConn;
    }
        
    public static Connection getProducaoConnection() throws Exception {
        Class.forName(ORACLE_DRIVER);
        String integraHost = PropertyUtil.getProperty(DB_PRODUCAO_HOST);
        String integraPort = PropertyUtil.getProperty(DB_PRODUCAO_PORT);
        String integraSid = PropertyUtil.getProperty(DB_PRODUCAO_SID);
        String integraUser = PropertyUtil.getProperty(DB_PRODUCAO_USER);
        String integraPasswd = PropertyUtil.getProperty(DB_PRODUCAO_PASSWORD);
        
        Connection integraConn = 
                DriverManager.getConnection("jdbc:oracle:thin:@" + integraHost + ":"+integraPort + "/" + integraSid,Util.decripta(integraUser), Util.decripta(integraPasswd));
        
        return integraConn;
    }
}

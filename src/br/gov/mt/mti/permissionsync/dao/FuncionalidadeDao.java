/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao;

import br.gov.mt.mti.permissionsync.dao.exception.ObjectAlreadyExistsException;
import br.gov.mt.mti.permissionsync.dominio.Situacao;
import br.gov.mt.mti.permissionsync.vo.Funcionalidade;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author c8757550
 */
public class FuncionalidadeDao extends AbstractDao<Funcionalidade> {
    
    public static final String FUNCIONALIDADE_ID = "FUNCIONALIDADE_ID";
    public static final String NOME = "NOME";
    public static final String DESCRICAO = "DESCRICAO";
    public static final String USR_SITUACAO = "USR_SITUACAO";    
    public static final String SUBSISTEMA = "TIPO_SUBSISTEMA";
    public static final String USR_ATRIBUICAO_FUNC = "USR_ATRIBUICAO_FUNC";
    
    public FuncionalidadeDao() throws Exception {
        super();
    }
    
    public FuncionalidadeDao(Connection conn){
        super(conn);
    }
    
    public void vincularPermissoes(Long funcionalidadeId, List<Permissao> listaPermissoes) throws Exception {
        try (Statement stmt = connection.createStatement()){
            stmt.execute("DELETE FROM FUNC_PERM WHERE FUNCIONALIDADE_ID = " + funcionalidadeId.toString());            
            connection.commit();
            String insertSQL = "INSERT INTO FUNC_PERM(FUNCIONALIDADE_ID, PERMISSAO_ID)"
                    + "VALUES(id1,id2)";
            for(Permissao permissao : listaPermissoes){
                stmt.execute(insertSQL.replace("id1", funcionalidadeId.toString())
                                      .replace("id2", permissao.getId().toString()));
            }
        }
    }
    
    public void excluir(Funcionalidade funcionalidade) throws Exception {
        try (Statement stmt = connection.createStatement()){
            //Deleta Funcionalidade da tabela de ligação de FUNCIONALIDADE C/ PERMISSÃO
            stmt.execute("DELETE FROM FUNC_PERM WHERE FUNCIONALIDADE_ID = " + funcionalidade.getId().toString());
            //Deleta Funcionalidade da tabela de ligação de PERFIL C/ FUNCIONALIDADE
            stmt.execute("DELETE FROM PERFIL_FUNCIONALIDADE WHERE FUNCIONALIDADE_ID = " + funcionalidade.getId().toString());
            //Deleta a Funcionalidade da tabela de Funcionalidade
            stmt.execute("DELETE FROM FUNCIONALIDADE WHERE FUNCIONALIDADE_ID = " + funcionalidade.getId().toString());
            connection.commit();            
        }
    }
    
    public Funcionalidade load(Long id) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = getSQLSelect(FUNCIONALIDADE_ID, NOME, DESCRICAO, USR_SITUACAO, SUBSISTEMA, 
                DATA_CRIACAO, DATA_ATUALIZACAO, USR_ATRIBUICAO_FUNC, VERSAO);        
        sql += " AND " + FUNCIONALIDADE_ID + " = ? ";
        
        System.out.println(sql);
        
        try {
            ps = connection.prepareStatement(sql);
            ps.setLong(1,id);
            
            rs = ps.executeQuery();
            
            if(rs.next()){                
                Funcionalidade f = getFromResultSet(rs);                
                return f;
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return null;
    }
    
    public List<Funcionalidade> top10() {
        List<Funcionalidade> lista = new ArrayList<>();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(getSQLSelectTOPN(10, FUNCIONALIDADE_ID, NOME, DESCRICAO, USR_SITUACAO, SUBSISTEMA,
                    USR_ATRIBUICAO_FUNC, DATA_CRIACAO, DATA_ATUALIZACAO, VERSAO));
        sql.append(" ORDER BY DATA_CRIACAO DESC");
        
        try {
            ps = connection.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            
            while(rs.next()){                
                Funcionalidade f = getFromResultSet(rs);                
                lista.add(f);
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return lista;
    }
    
    public List<Funcionalidade> list(final Funcionalidade funcionalidade) {
        List<Funcionalidade> lista = new ArrayList<>();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(getSQLSelect(FUNCIONALIDADE_ID, NOME, DESCRICAO, USR_SITUACAO, SUBSISTEMA,
                    USR_ATRIBUICAO_FUNC, DATA_CRIACAO, DATA_ATUALIZACAO, VERSAO));
        sql.append("AND UPPER(NOME) LIKE UPPER(?) ");
        sql.append("AND USR_SITUACAO IN (:SITUACAO) ");
        sql.append("ORDER BY NOME ASC");
        
        try {
            StringBuilder whereSituacao = new StringBuilder();
            if(funcionalidade.getSituacao() == null){
                whereSituacao.append(Situacao.ATIVO.getCod())
                             .append(",")
                             .append(Situacao.INATIVO.getCod());
            } else {
                whereSituacao.append(funcionalidade.getSituacao().getCod());
            }            
            
            ps = connection.prepareStatement(sql.toString().replace(":SITUACAO", whereSituacao.toString()));
            if(funcionalidade.getNome() == null || funcionalidade.getNome().isEmpty()){
                ps.setString(1,"%");
            } else {
                ps.setString(1, funcionalidade.getNome());
            }
            
            rs = ps.executeQuery();
            
            while(rs.next()){                
                Funcionalidade f = getFromResultSet(rs);                
                lista.add(f);
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return lista;
    }
    
    public boolean isFuncionalidadeCadastrada(final Funcionalidade funcionalidade) throws ObjectAlreadyExistsException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(1) QTDE FROM ").append(getTableName()).append(" WHERE LOWER(NOME) = LOWER(?) ");
        if(funcionalidade.getId() != null){
            sql.append("AND ").append(getIdColumn()).append("<>").append(funcionalidade.getId().toString());
        }
        
        try {
            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, funcionalidade.getNome().trim());
            rs = ps.executeQuery();
            
            if(rs.next()){
                if(rs.getInt("QTDE") > 0){
                    throw new ObjectAlreadyExistsException("Funcionalidade já cadastrada.");
                }
            }
            
        } catch(SQLException e){
        } finally {
            close(ps, rs);            
        }
        
        return false;
    }
    
    public void persist(final Funcionalidade funcionalidade) throws ObjectAlreadyExistsException, SQLException {
        PreparedStatement ps = null;
        Long id = getMaxID() + 1;        
        
        isFuncionalidadeCadastrada(funcionalidade);
        
        try {            
            ps = connection.prepareStatement(getSQLInsert(FUNCIONALIDADE_ID, NOME, DESCRICAO, USR_SITUACAO, SUBSISTEMA, DATA_CRIACAO, VERSAO));
            ps.setLong(1,id);
            ps.setString(2, funcionalidade.getNome().trim());
            ps.setString(3, funcionalidade.getDescricao().trim());
            ps.setInt(4, funcionalidade.getSituacao().getCod());  
            if(funcionalidade.getSubsistema() == null){
                ps.setNull(5, Types.NULL);
            } else {
                ps.setInt(5, funcionalidade.getSubsistema().getCod());
            }
            ps.setDate(6, getDataAtual());
            ps.setInt(7, 0);
            ps.execute();
            connection.commit();
            //Incluir funcionalidade já vinculando ao perfil MASTER
            ps = connection.prepareStatement("INSERT INTO PERFIL_FUNCIONALIDADE (PERFIL_ID, FUNCIONALIDADE_ID) VALUES(?,?)");
            ps.setLong(1,1L);
            ps.setLong(2, id);
            ps.execute();
            connection.commit();
        } catch(SQLException e){
            throw e;
        } finally {
            close(ps);
        }
    }
    
    public void update(final Funcionalidade funcionalidade) throws ObjectAlreadyExistsException {
        PreparedStatement ps = null;
        Long id = getMaxID() + 1;        
        
        isFuncionalidadeCadastrada(funcionalidade);
        
        try {            
            ps = connection.prepareStatement(getSQLUpdate(funcionalidade.getId(), FUNCIONALIDADE_ID, NOME, DESCRICAO, USR_SITUACAO, SUBSISTEMA, DATA_ATUALIZACAO));
            ps.setLong(1,id);
            ps.setString(2, funcionalidade.getNome().trim());
            ps.setString(3, funcionalidade.getDescricao().trim());
            ps.setInt(4, funcionalidade.getSituacao().getCod());            
            if(funcionalidade.getSubsistema() == null){
                ps.setNull(5, Types.NULL);            
            } else {
                ps.setInt(5, funcionalidade.getSubsistema().getCod());            
            }
            ps.setDate(6, getDataAtual());    
            ps.execute();
            connection.commit();
        } catch(SQLException e){
        } finally {
            close(ps);
        }
    }

    @Override
    public Funcionalidade getFromResultSet(ResultSet rs) throws SQLException{
        
        Funcionalidade f = new Funcionalidade(rs.getString(NOME), rs.getString(DESCRICAO), 
                getSituacaoFromResultSet(rs, USR_SITUACAO), getSubsistemaFromResultSet(rs, SUBSISTEMA));                
        f.setDataCriacao(new java.util.Date(rs.getDate(DATA_CRIACAO).getTime()));
        
        if(rs.getDate(DATA_ATUALIZACAO) != null){
            f.setDataAtualizacao(new java.util.Date(rs.getDate(DATA_ATUALIZACAO).getTime()));
        }
        
        f.setVersao(rs.getInt(VERSAO));
        f.setUsrAtribuicaoFunc(rs.getLong(USR_ATRIBUICAO_FUNC));
        f.setId(rs.getLong(FUNCIONALIDADE_ID));
        
        return f;
            
    }

    @Override
    public String getTableName() {
        return "FUNCIONALIDADE";
    }

    @Override
    public String getIdColumn() {
        return FUNCIONALIDADE_ID;
    }
}

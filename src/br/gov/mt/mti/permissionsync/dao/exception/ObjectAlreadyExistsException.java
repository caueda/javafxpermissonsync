/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao.exception;

/**
 *
 * @author c8757550
 */
public class ObjectAlreadyExistsException extends Exception {
    public ObjectAlreadyExistsException(){
        super();
    }
    
    public ObjectAlreadyExistsException(String message){
        super(message);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao;

import br.gov.mt.mti.permissionsync.dao.interfaces.Importable;
import br.gov.mt.mti.permissionsync.vo.BaseModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author c8757550
 */
public class ImportDao extends AbstractDao {
    
    public ImportDao() throws Exception {
        super();
    }
    
    Importable carregarPermissao = (conn1, conn2) -> {
        int inserted = 0;
        int error = 0;
        String sqlFromIntegra = "SELECT PERMISSAO_ID,NOME,ACTION,DESCRICAO,TIPO_PERMISSAO,DATA_CRIACAO,DATA_ATUALIZACAO,VERSAO FROM BCNTB0612 ORDER BY PERMISSAO_ID";
        try {            
            String insertSQL = "INSERT INTO PERMISSAO(PERMISSAO_ID,TIPO_PERMISSAO,NOME,DESCRICAO,ACTION,DATA_CRIACAO,DATA_ATUALIZACAO,VERSAO)"
                    + "VALUES(?,?,?,?,?,?,?,?)";
            try(PreparedStatement insert = conn2.prepareStatement(insertSQL)){
                try(PreparedStatement ps = conn1.prepareStatement(sqlFromIntegra);
                    Statement stmt = conn2.createStatement()){
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        Long permissaoId = rs.getLong("PERMISSAO_ID");
                        ResultSet rss = stmt.executeQuery("SELECT COUNT(1) QTDE FROM PERMISSAO WHERE PERMISSAO_ID = " + permissaoId.toString());
                        int qtde = 0;
                        if(rss.next()){
                            qtde = rss.getInt("QTDE");
                        }
                        
                        if(qtde > 0) continue;
                        
                        insert.setLong(1,permissaoId);
                        insert.setInt(2, rs.getInt("TIPO_PERMISSAO"));
                        insert.setString(3, rs.getString("NOME"));
                        insert.setString(4, rs.getString("DESCRICAO"));
                        insert.setString(5, rs.getString("ACTION"));
                        insert.setDate(6, rs.getDate("DATA_CRIACAO"));
                        insert.setDate(7, rs.getDate("DATA_ATUALIZACAO"));
                        insert.setInt(8, rs.getInt("VERSAO"));
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        conn2.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        } finally {
        }
        System.out.println("Nº de Permissões inseridas: " + inserted);
        System.out.println("Nº de Permissões (Erros): " + error);
    };
    
    Importable carregarFuncionalidade = (conn1, conn2) -> {
        int inserted = 0;
        int error = 0;
        String sqlFromIntegra = "SELECT FUNCIONALIDADE_ID, NOME, DESCRICAO,\n" +
                                "       TIPO_SUBSISTEMA, USR_ATRIBUICAO_FUNC,\n" +
                                "       DATA_CRIACAO, DATA_ATUALIZACAO,\n" +
                                "       USR_SITUACAO,VERSAO\n" +
                                "  FROM BCNTB0613\n" +
                                " ORDER BY FUNCIONALIDADE_ID";
        try {            
            String insertSQL = "INSERT INTO FUNCIONALIDADE(FUNCIONALIDADE_ID, NOME, DESCRICAO, TIPO_SUBSISTEMA, USR_ATRIBUICAO_FUNC, DATA_CRIACAO, DATA_ATUALIZACAO,"
                    + "USR_SITUACAO, VERSAO)"
                    + "VALUES(?,?,?,?,?,?,?,?,?)";
            try(PreparedStatement insert = conn2.prepareStatement(insertSQL)){
                try(PreparedStatement ps = conn1.prepareStatement(sqlFromIntegra);
                    Statement stmt = conn2.createStatement()){
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        Long funcionalidadeId = rs.getLong("FUNCIONALIDADE_ID");
                        int qtde = 0;
                        ResultSet rss = stmt.executeQuery("SELECT COUNT(1) QTDE FROM FUNCIONALIDADE WHERE FUNCIONALIDADE_ID = " + funcionalidadeId.toString());
                        if(rss.next()){
                            qtde = rss.getInt("QTDE");
                            
                        }                        
                        if(qtde > 0) continue;
                        
                        insert.setLong(1, funcionalidadeId);
                        insert.setString(2, rs.getString("NOME"));
                        insert.setString(3, rs.getString("DESCRICAO"));
                        insert.setInt(4, rs.getInt("TIPO_SUBSISTEMA"));
                        insert.setLong(5, rs.getLong("USR_ATRIBUICAO_FUNC"));
                        insert.setDate(6, rs.getDate("DATA_CRIACAO"));
                        insert.setDate(7, rs.getDate("DATA_CRIACAO"));
                        insert.setInt(8, rs.getInt("USR_SITUACAO"));
                        insert.setInt(9, rs.getInt("VERSAO"));
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        conn2.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        } finally {
        }
        System.out.println("Nº de Funcionalidades inseridas: " + inserted);
        System.out.println("Nº de Funcionalidades (Erros): " + error);
    };
    
    Importable carregarFuncPerm = (conn1, conn2) -> {
        int inserted = 0;
        int error = 0;
        String sqlFromIntegra = "SELECT FUNCIONALIDADE_ID, PERMISSAO_ID FROM BCNTB0615 ORDER BY FUNCIONALIDADE_ID";
        try {            
            String insertSQL = "INSERT INTO FUNC_PERM(FUNCIONALIDADE_ID, PERMISSAO_ID) VALUES(?,?)";
            
            try(PreparedStatement insert = conn2.prepareStatement(insertSQL)){
                try(PreparedStatement ps = conn1.prepareCall(sqlFromIntegra);
                    Statement stmt = conn2.createStatement()){
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        Long funcionalidadeId = rs.getLong("FUNCIONALIDADE_ID");
                        Long permissaoId = rs.getLong("PERMISSAO_ID");
                        int qtde = 0;
                        ResultSet rss = stmt.executeQuery("SELECT COUNT(1) QTDE FROM FUNC_PERM WHERE FUNCIONALIDADE_ID=" + funcionalidadeId.toString() + " AND "
                                + "PERMISSAO_ID=" + permissaoId.toString());
                        if(rss.next()){
                            qtde = rss.getInt("QTDE");
                        }
                        if(qtde > 0) continue;
                        insert.setLong(1,funcionalidadeId);
                        insert.setLong(2, permissaoId);
                        
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        conn2.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        } finally {
        }
        System.out.println("Nº de FuncPerm inseridas: " + inserted);
        System.out.println("Nº de FuncPerm (Erros): " + error);
    };
    
    Importable carregarPerfil = (conn1, conn2) -> {
        int inserted = 0;
        int error = 0;
        String sqlFromIntegra = "SELECT PERFIL_ID,NOME,DESCRICAO,USR_SITUACAO,DATA_CRIACAO,DATA_ATUALIZACAO,VERSAO FROM BCNTB0611 WHERE PERFIL_ID = 1";
        try {            
            String insertSQL = "INSERT INTO PERFIL(PERFIL_ID,NOME,DESCRICAO,USR_SITUACAO,DATA_CRIACAO,DATA_ATUALIZACAO,VERSAO) "
                    + "VALUES(?,?,?,?,?,?,?)";
            
            try(PreparedStatement insert = conn2.prepareStatement(insertSQL)){
                try(PreparedStatement ps = conn1.prepareCall(sqlFromIntegra);
                    Statement stmt = conn2.createStatement()){
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        Long perfilId = rs.getLong("PERFIL_ID");
                        int qtde = 0;
                        ResultSet rss = stmt.executeQuery("SELECT COUNT(1) QTDE FROM PERFIL WHERE PERFIL_ID = "+ perfilId.toString());
                        if(rss.next()){
                            qtde = rss.getInt("QTDE");
                        }
                        if(qtde > 0) continue;
                        
                        insert.setLong(1,perfilId);
                        insert.setString(2, rs.getString("NOME"));
                        insert.setString(3, rs.getString("DESCRICAO"));
                        insert.setInt(4, rs.getInt("USR_SITUACAO"));
                        insert.setDate(5, rs.getDate("DATA_CRIACAO"));
                        insert.setDate(6, rs.getDate("DATA_ATUALIZACAO"));
                        insert.setInt(7, rs.getInt("VERSAO"));
                        
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        conn2.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        } finally {
        }
        System.out.println("Nº de Perfil inseridas: " + inserted);
        System.out.println("Nº de Perfil (Erros): " + error);
    };
    
    Importable carregarPerfilFuncionalidade = (conn1, conn2) -> {
        int inserted = 0;
        int error = 0;
        String sqlFromIntegra = "SELECT PERFIL_ID,FUNCIONALIDADE_ID FROM BCNTB0614 WHERE PERFIL_ID = 1 ORDER BY FUNCIONALIDADE_ID";
        try {            
            String insertSQL = "INSERT INTO PERFIL_FUNCIONALIDADE(PERFIL_ID, FUNCIONALIDADE_ID) "
                    + "VALUES(?,?)";
            
            try(PreparedStatement insert = conn2.prepareStatement(insertSQL)){
                try(PreparedStatement ps = conn1.prepareCall(sqlFromIntegra);
                    Statement stmt = conn2.createStatement()){
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        Long perfilId = rs.getLong("PERFIL_ID");
                        Long funcionalidadeId = rs.getLong("FUNCIONALIDADE_ID");
                        int qtde = 0;
                        ResultSet rss = stmt.executeQuery("SELECT COUNT(1) QTDE FROM PERFIL_FUNCIONALIDADE WHERE PERFIL_ID=1 AND FUNCIONALIDADE_ID=" + funcionalidadeId.toString());
                        if(rss.next()){
                            qtde = rss.getInt("QTDE");
                        }
                        if(qtde > 0) continue;
                        insert.setLong(1,perfilId);                        
                        insert.setLong(2,funcionalidadeId);
                        
                        if(insert.executeUpdate() > 1){
                            inserted++;
                        } else {
                            error++;
                        }
                        conn2.commit();
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        } finally {
        }
        System.out.println("Nº de Perfil Funcionalidade inseridas: " + inserted);
        System.out.println("Nº de Perfil Funcionalidade (Erros): " + error);
    };

    @Override
    public BaseModel getFromResultSet(ResultSet rs) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTableName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getIdColumn() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void executar() throws Exception { 
        Connection connOrigem = null;
        try {
            connOrigem = getIntegraConnection();
            carregarPermissao.execute(connOrigem, connection);
            carregarFuncionalidade.execute(connOrigem, connection);
            carregarFuncPerm.execute(connOrigem, connection);
            carregarPerfil.execute(connOrigem, connection);
            carregarPerfilFuncionalidade.execute(connOrigem, connection);
        } finally {
            close(connOrigem, connection);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao;

import br.gov.mt.mti.permissionsync.dao.exception.ObjectAlreadyExistsException;
import br.gov.mt.mti.permissionsync.dominio.TipoPermissao;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.io.BufferedReader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author c8757550
 */
public class PermissaoDao extends AbstractDao<Permissao> {
    
    public static final String PERMISSAO_ID = "PERMISSAO_ID";
    public static final String TIPO_PERMISSAO = "TIPO_PERMISSAO";
    public static final String NOME = "NOME";
    public static final String DESCRICAO = "DESCRICAO";
    public static final String ACTION = "ACTION";
    
    public PermissaoDao() throws Exception {
        super();
    }
    
    public PermissaoDao(Connection conn){
        super(conn);
    }
    
    public List<Permissao> top10() {
        List<Permissao> lista = new ArrayList<>();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(getSQLSelectTOPN(10, PERMISSAO_ID, TIPO_PERMISSAO, NOME, DESCRICAO, ACTION, DATA_CRIACAO, DATA_ATUALIZACAO, VERSAO));
        sql.append(" ORDER BY DATA_CRIACAO DESC");
        
        try {
            ps = connection.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            
            while(rs.next()){                
                Permissao p = getFromResultSet(rs);                
                lista.add(p);
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return lista;
    }
    
    public void excluir(Permissao permissao) throws Exception {
        try (Statement stmt = connection.createStatement()){
            //Deleta Permissão da tabela de ligação de FUNCIONALIDADE C/ PERMISSÃO
            stmt.execute("DELETE FROM FUNC_PERM WHERE PERMISSAO_ID = " + permissao.getId().toString());            
            //Deleta a Funcionalidade da tabela de Funcionalidade
            stmt.execute("DELETE FROM PERMISSAO WHERE PERMISSAO_ID = " + permissao.getId().toString());
            connection.commit();            
        }
    }
    
    public Permissao load(Long id) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql.append(getSQLSelect(PERMISSAO_ID, NOME, DESCRICAO, ACTION, TIPO_PERMISSAO, DATA_CRIACAO, DATA_ATUALIZACAO, VERSAO));
        sql.append(" AND " + PERMISSAO_ID + "=?");
        
        try {
            ps = connection.prepareStatement(sql.toString());
            ps.setLong(1,id);
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                Permissao permissao = getFromResultSet(rs);
                return permissao;
            }
            
        } catch(SQLException e){
            e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return null;
    }
    
    public List<Permissao> list(final Permissao permissao) {
        List<Permissao> lista = new ArrayList<>();
        
        String paramAction = (permissao.getAction() == null || permissao.getAction().isEmpty()) ? "%" : permissao.getAction();
        
        BufferedReader reader = new BufferedReader(new StringReader(paramAction));
        String linha = null;
        StringBuilder actionCondition = new StringBuilder(" AND (");
        List<String> parametros = new ArrayList<String>();
        try {
            while((linha=reader.readLine()) != null){
                parametros.add(linha);
            }
        } catch(Exception e){}
        for(int i=0; i<parametros.size(); i++){
            if(i==0){
                actionCondition.append(" UPPER(ACTION) LIKE '")
                               .append(parametros.get(i))
                               .append("' ");                
            } else {
                actionCondition.append(" OR UPPER(ACTION) LIKE '")
                               .append(parametros.get(i))
                               .append("' ");
            }
        }
        actionCondition.append(") ");
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(getSQLSelect(PERMISSAO_ID, NOME, DESCRICAO, 
                TIPO_PERMISSAO, ACTION, DATA_CRIACAO, DATA_ATUALIZACAO, VERSAO));
        sql.append(actionCondition.toString());
        sql.append("AND TIPO_PERMISSAO IN (:P_TIPO) ");
        sql.append("ORDER BY ACTION ASC");
        
        try {
            StringBuilder whereTipoPermissao = new StringBuilder();
            if(permissao.getTipoPermissao() == null){
                whereTipoPermissao.append(TipoPermissao.ACAO.getCod())
                             .append(",")
                             .append(TipoPermissao.GATILHO.getCod())
                             .append(",")
                             .append(TipoPermissao.PAPEL.getCod());
            } else {
                whereTipoPermissao.append(permissao.getTipoPermissao().getCod());
            }            
            String statement  = sql.toString().replace(":P_TIPO", whereTipoPermissao.toString());
            
            System.out.println(statement);
            
            ps = connection.prepareStatement(statement);
            
            rs = ps.executeQuery();
            
            while(rs.next()){                
                Permissao p = getFromResultSet(rs);                
                lista.add(p);
            }
            
        } catch(SQLException e){
          e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return lista;
    }
    
    public List<Permissao> listByFuncionalidadeId(Long funcionalidadeId) {
        List<Permissao> lista = new ArrayList<>();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        
        sql.append("SELECT P.PERMISSAO_ID, P.NOME, P.DESCRICAO, P.TIPO_PERMISSAO, P.ACTION, P.DATA_CRIACAO, P.DATA_ATUALIZACAO, P.VERSAO ");
        sql.append(" FROM PERMISSAO P JOIN FUNC_PERM FP ON P.PERMISSAO_ID = FP.PERMISSAO_ID ");
        sql.append(" WHERE FP.FUNCIONALIDADE_ID = ? ");
        sql.append(" ORDER BY PERMISSAO_ID ");
        
        try {            
            ps = connection.prepareStatement(sql.toString());            
            ps.setLong(1, funcionalidadeId);
            
            rs = ps.executeQuery();
            
            while(rs.next()){                
                Permissao p = getFromResultSet(rs);                
                lista.add(p);
            }
            
        } catch(SQLException e){
          e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return lista;
    }
    
    public boolean isPermissaoCadastrada(final Permissao permissao) throws ObjectAlreadyExistsException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(1) QTDE FROM ").append(getTableName()).append(" WHERE LOWER(" + ACTION + ") = LOWER(?) ");
        if(permissao.getId() != null){
            sql.append(" AND ").append(getIdColumn()).append("<>").append(permissao.getId().toString());
        }
        
        try {
            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, permissao.getAction().trim());
            rs = ps.executeQuery();
            
            if(rs.next()){
                if(rs.getInt("QTDE") > 0){
                    throw new ObjectAlreadyExistsException("Permissão já cadastrada.");
                }
            }
            
        } catch(SQLException e){
            //
        } finally {
            close(ps, rs);            
        }
        
        return false;
    }
       
    public void persist(final Permissao permissao) throws ObjectAlreadyExistsException, SQLException{
        PreparedStatement ps = null;
        Long id = getMaxID() + 1;
        String sql = getSQLInsert(PERMISSAO_ID, TIPO_PERMISSAO, NOME, DESCRICAO, ACTION, DATA_CRIACAO);        
        
        isPermissaoCadastrada(permissao);
        
        try {
            ps = connection.prepareStatement(sql);
            ps.setLong(1,id);
            ps.setInt(2, permissao.getTipoPermissao().getCod());
            ps.setString(3, permissao.getNome().trim());
            ps.setString(4, permissao.getDescricao().trim());
            ps.setString(5, permissao.getAction().trim());
            ps.setDate(6, getDataAtual());
            ps.execute();
            connection.commit();
        } catch(SQLException e){  
            throw e;
        } finally {
            close(ps);
        }
    }
    
    public void update(final Permissao permissao) throws ObjectAlreadyExistsException {
        PreparedStatement ps = null;
        Long id = getMaxID() + 1;        
        
        isPermissaoCadastrada(permissao);
        
        try {            
            ps = connection.prepareStatement(getSQLUpdate(permissao.getId(), PERMISSAO_ID, NOME, DESCRICAO, ACTION, TIPO_PERMISSAO, DATA_ATUALIZACAO));
            ps.setLong(1,id);
            ps.setString(2, permissao.getNome().trim());
            ps.setString(3, permissao.getDescricao().trim());
            ps.setString(4, permissao.getAction().trim());            
            ps.setInt(5, permissao.getTipoPermissao().getCod());            
            ps.setDate(6, getDataAtual());    
            ps.execute();
            connection.commit();
        } catch(SQLException e){
        } finally {
            close(ps);
        }
    }

    @Override
    public Permissao getFromResultSet(ResultSet rs) throws SQLException {
        Permissao p = new Permissao();        
        p.setId(rs.getLong(PERMISSAO_ID));
        p.setNome(rs.getString(NOME));
        p.setDescricao(rs.getString(DESCRICAO));
        p.setAction(rs.getString(ACTION));
        p.setTipoPermissao(getTipoPermissaoFromResultSet(rs, TIPO_PERMISSAO));
        p.setDataCriacao(new java.util.Date(rs.getDate(DATA_CRIACAO).getTime()));
        if(rs.getDate(DATA_ATUALIZACAO) != null){
            p.setDataAtualizacao(new java.util.Date(rs.getDate(DATA_ATUALIZACAO).getTime()));
        }
        return p;
    }

    @Override
    public String getTableName() {
        return "PERMISSAO";
    }

    @Override
    public String getIdColumn() {
        return PERMISSAO_ID;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dao;

import static br.gov.mt.mti.permissionsync.dao.AbstractDao.DATA_ATUALIZACAO;
import static br.gov.mt.mti.permissionsync.dao.AbstractDao.DATA_CRIACAO;
import static br.gov.mt.mti.permissionsync.dao.AbstractDao.VERSAO;
import br.gov.mt.mti.permissionsync.dao.exception.ObjectAlreadyExistsException;
import br.gov.mt.mti.permissionsync.vo.Perfil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author c8757550
 */
public class PerfilDao extends AbstractDao<Perfil> {
    
    public static final String PERFIL_ID = "PERFIL_ID";
    public static final String NOME = "NOME";
    public static final String DESCRICAO = "DESCRICAO";
    public static final String USR_SITUACAO = "USR_SITUACAO";
    
    public PerfilDao() throws Exception {
        super();
    }
    
    public PerfilDao(Connection conn) throws Exception {
        super(conn);
    }
    
    public List<Perfil> top10() {
        List<Perfil> lista = new ArrayList<>();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(getSQLSelectTOPN(10, PERFIL_ID, NOME, DESCRICAO, USR_SITUACAO, DATA_CRIACAO, DATA_ATUALIZACAO, VERSAO));
        sql.append(" ORDER BY DATA_CRIACAO DESC");
        
        try {
            ps = connection.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            
            while(rs.next()){                
                Perfil f = getFromResultSet(rs);                
                lista.add(f);
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
        } finally {
            close(ps, rs);            
        }
        
        return lista;
    }
    
    public boolean isPerfilCadastrado(final Perfil perfil) throws ObjectAlreadyExistsException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(1) QTDE FROM ").append(getTableName()).append(" WHERE LOWER(NOME) = LOWER(?)");
        
        try {
            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, perfil.getNome().trim());
            rs = ps.executeQuery();
            
            if(rs.next()){
                if(rs.getInt("QTDE") > 0){
                    throw new ObjectAlreadyExistsException("Perfil já cadastrado.");
                }
            }
            
        } catch(SQLException e){
            //
        } finally {
            close(ps, rs);            
        }
        
        return false;
    }
       
    public void persist(final Perfil perfil) throws ObjectAlreadyExistsException{
        PreparedStatement ps = null;
        Long id = getMaxID() + 1;
        String sql = getSQLInsert(PERFIL_ID, NOME, DESCRICAO, USR_SITUACAO, DATA_CRIACAO);
                
        isPerfilCadastrado(perfil);
        
        try {
            ps = connection.prepareStatement(sql);
            ps.setLong(1,id);
            ps.setString(2, perfil.getNome());            
            ps.setString(3, perfil.getDescricao());
            ps.setInt(4, perfil.getSituacao().getCod());
            ps.setDate(5, getDataAtual());
            ps.execute();
            connection.commit();
        } catch(SQLException e){            
        } finally {
            close(ps);
        }
    }

    @Override
    public Perfil getFromResultSet(ResultSet rs) throws SQLException {
        Perfil p = new Perfil();
        p.setId(rs.getLong(PERFIL_ID));
        p.setNome(rs.getString(NOME));
        p.setDescricao(rs.getString(DESCRICAO));
        p.setSituacao(getSituacaoFromResultSet(rs, USR_SITUACAO));        
        p.setDataCriacao(new java.util.Date(rs.getDate(DATA_CRIACAO).getTime()));        
        if(rs.getDate(DATA_ATUALIZACAO) != null){
            p.setDataAtualizacao(new java.util.Date(rs.getDate(DATA_ATUALIZACAO).getTime()));
        }        
        return p;
    }

    @Override
    public String getTableName() {
        return "PERFIL";
    }

    @Override
    public String getIdColumn() {
        return PERFIL_ID;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.util;

import br.gov.mt.mti.permissionsync.dao.interfaces.Constantes;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author c8757550
 */
public class PropertyUtil implements Constantes {
    
    private static final Properties PROP = new Properties();    
    
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/YYYY");
    
    public static final String getDataFormatada(Date data){
        try {
            return SDF.format(data);
        } catch(Exception e){}
        return "";
    }
    
    public static final String getDataFormatada(Date data, String mask){
        try {
            SDF.applyPattern(mask);
            String formatted = SDF.format(data);
            SDF.applyPattern("dd/MM/yyyy");
            return formatted;
        } catch(Exception e){}
        return "";
    }
    
    static {
        String directory = System.getProperty("user.dir");
        try {
            InputStream in = new FileInputStream(directory + "/config.properties");
            PROP.load(in);            
            in.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static String getProperty(String property){
        return PROP.getProperty(property);
    }
}

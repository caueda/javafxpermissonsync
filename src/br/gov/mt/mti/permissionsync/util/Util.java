/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.util;

import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author c8757550
 */
public class Util {
    private static final String myEncryptionKey = "chave de 16bytes";
	private static final String UNICODE_FORMAT = "UTF8";

	public static String encripta(String id) {
		String retorno = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

			// Usando chave de 128-bits (16 bytes)
			byte[] chave = myEncryptionKey.getBytes();

			// Encriptando...
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"));
			byte[] encrypted = cipher.doFinal(id
					.getBytes(UNICODE_FORMAT));

			//retorno = new BASE64Encoder().encode(encrypted);
                        
			retorno = new String(Base64.getEncoder().encode(encrypted));
			return retorno;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String decripta(String id) {
		if (id == null) {
			return null;
		}
		String retorno = null;
		
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			// Usando chave de 128-bits (16 bytes)
			byte[] chave = myEncryptionKey.getBytes();

			// Decriptando...
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(chave, "AES"));
                        
			byte[] decrypted  = Base64.getDecoder().decode(id);
		//	byte[] decrypted = new BASE64Decoder().decodeBuffer(id);

			// System.out.println(new String(decrypted));
			
			retorno = new String(cipher.doFinal(decrypted));
			//retorno = ;
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			retorno= id;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try{
			return retorno;
		}catch (NumberFormatException e)
		{
			e.printStackTrace();
			return null;
		}

	}
        /*
        public static void main(String[] args){
            String senha = "sindesa_hom";
            String encript = encripta(senha);
            System.out.println(encript);
            System.out.println(decripta("ikR34fxIDe4lUMwsW88uew=="));
        }
        */
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.FuncionalidadeDao;
import br.gov.mt.mti.permissionsync.dao.ImportDao;
import br.gov.mt.mti.permissionsync.dominio.Situacao;
import br.gov.mt.mti.permissionsync.vo.Funcionalidade;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author c8757550
 */
public class FXMLDocumentController extends AbstractController<Funcionalidade> implements Initializable {

    @FXML
    private MenuItem menuPermissao;
    @FXML
    private MenuItem menuPermissaoPesquisar;
    @FXML
    private MenuItem menuSobre;
    @FXML
    private MenuItem menuFuncionalidade;
    @FXML
    private MenuItem menuImport;
    @FXML
    private MenuItem menuSincronizar;
    @FXML
    private TextField pesquisaNome;
    @FXML
    private ComboBox<Situacao> pesquisaComboSituacao;
    @FXML
    private TableView<Funcionalidade> tableFunc;
    @FXML
    private TableColumn<Funcionalidade,String> nome;
    @FXML
    private TableColumn<Funcionalidade,String> descricao;
    @FXML
    private TableColumn<Funcionalidade,String> subsistemaGrid;
    @FXML
    private TableColumn<Funcionalidade,Enum> situacao;
    @FXML
    private TableColumn<Funcionalidade,Date> dataCriacao;
    @FXML
    private TableColumn<Funcionalidade,Long> usrAtribuicaoFunc;
    @FXML
    private TableColumn<Funcionalidade,Date> dataAtualizacao;
    @FXML
    private TableColumn<Funcionalidade,Integer> versao;
    
    private final ObservableList<Funcionalidade> listaDeFuncionalidades =
                FXCollections.observableArrayList();
    
    private Funcionalidade funcionalidadeSelecionada;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        descricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));        
        subsistemaGrid.setCellValueFactory(new PropertyValueFactory<>("subsistema"));
        situacao.setCellValueFactory(new PropertyValueFactory<>("situacao"));
        usrAtribuicaoFunc.setCellValueFactory(new PropertyValueFactory<>("usrAtribuicaoFunc"));
        dataCriacao.setCellValueFactory(new PropertyValueFactory<>("dataCriacaoFormatada"));
        dataAtualizacao.setCellValueFactory(new PropertyValueFactory<>("dataAtualizacaoFormatada"));
        versao.setCellValueFactory(new PropertyValueFactory<>("versao"));
        
        pesquisaComboSituacao.getItems().addAll(Situacao.ATIVO, Situacao.INATIVO);
        pesquisaComboSituacao.setEditable(false);        
        
        setTitulo("Funcionalidades");
        
        FuncionalidadeDao dao = null;
        List<Funcionalidade> listaFuncionalidade = new ArrayList<>();
        
        try {
            dao = new FuncionalidadeDao();
            listaFuncionalidade.addAll(dao.top10());
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }         
        
        listaDeFuncionalidades.addAll(listaFuncionalidade);
        
        tableFunc.setItems(listaDeFuncionalidades);
        
        tableFunc.setRowFactory(obj -> {
            final ContextMenu rowMenu = new ContextMenu();
            MenuItem editItem = new MenuItem("Editar");
            MenuItem showPermissoesItem = new MenuItem("Mostrar permissões");
            MenuItem excluirItem = new MenuItem("Excluir");
            
            editItem.setOnAction(event -> {
                abrirFormFuncionalidade(getFuncionalidadeSelecionada().getId());
            });            
            showPermissoesItem.setOnAction(event -> {
                abrirFormFuncionalidadePermissao(getFuncionalidadeSelecionada().getId());
            });
            excluirItem.setOnAction(e -> {
                excluir(getFuncionalidadeSelecionada());
            });
            rowMenu.getItems().add(editItem);
            rowMenu.getItems().add(excluirItem);
            rowMenu.getItems().add(showPermissoesItem);
            
            TableRow<Funcionalidade> row = new TableRow();
            row.contextMenuProperty().bind(
              Bindings.when(Bindings.isNotNull(row.itemProperty()))
              .then(rowMenu)
              .otherwise((ContextMenu)null));
            
            row.setOnMouseClicked(event ->{
                setFuncionalidadeSelecionada(row.getItem());
                if(event.getClickCount() == 2 && !row.isEmpty()){
                    abrirFormFuncionalidadePermissao(getFuncionalidadeSelecionada().getId());
                }
            });
            return row;
        });
        
        menuPermissao.setOnAction((t) -> {
            abrirFormPermissao(null);
        });
        
        menuPermissaoPesquisar.setOnAction((t) -> {
            abrirFormPermissaoPesquisar();
        });

        menuFuncionalidade.setOnAction((t) -> {
            abrirFormFuncionalidade(null);
        });

        menuSobre.setOnAction((t) -> {
            abrirFormSobre();
        });
        
        menuImport.setOnAction(t -> {
            ImportDao daoImport = null;
            try {
                daoImport  = new ImportDao();
                daoImport.executar();
            } catch(Exception e){
                showException(e);
                return;
            } finally {
                close(daoImport);
            }
            showSucesso("Registros importados com sucesso");
        });
        
        menuSincronizar.setOnAction(t -> {
            abrirFormSincronizar(null);
        });
    }

    @FXML
    public void pesquisar(ActionEvent event){
        Funcionalidade pesquisar = new Funcionalidade();
        pesquisar.setNome(pesquisaNome.getText());
        pesquisar.setSituacao(pesquisaComboSituacao.getValue());
        listaDeFuncionalidades.clear();
        
        FuncionalidadeDao dao = null;
        
        try {
            dao = new FuncionalidadeDao();
            listaDeFuncionalidades.addAll(dao.list(pesquisar));
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }
    }
    
    public void excluir(Funcionalidade funcionalidade){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmação");
        alert.setHeaderText("Deseja realmente excluir a Funcionalidade ?");
        alert.setContentText("Você estará excluindo a funcionalidade do sistema.\n"
                + "Deseja continuar com a exclusão?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            FuncionalidadeDao dao = null;
            try {
                dao = new FuncionalidadeDao();
                dao.excluir(funcionalidade);
                this.listaDeFuncionalidades.remove(funcionalidade);
                showSucesso("Funcionalidade excluída com sucesso!");
            } catch(Exception e){
                showException(e);
            } finally {
                close(dao);
            }
        }
    }
    
    @FXML
    public void limparForm(ActionEvent event){
        pesquisaNome.setText(null);
        pesquisaComboSituacao.setValue(null);
        listaDeFuncionalidades.clear();
    }
    
    @Override
    public Funcionalidade carregar(Long id) {
        return null;
    }

    public Funcionalidade getFuncionalidadeSelecionada() {
        return funcionalidadeSelecionada;
    }

    public void setFuncionalidadeSelecionada(Funcionalidade funcionalidadeSelecionada) {
        this.funcionalidadeSelecionada = funcionalidadeSelecionada;
    }  
}

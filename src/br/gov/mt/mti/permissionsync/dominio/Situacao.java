/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dominio;

/**
 *
 * @author c8757550
 */
public enum Situacao {
    ATIVO(3), INATIVO(4);
    int cod;

    Situacao(int cod) {
        this.cod = cod;
    }
    
    public int getCod(){
        return this.cod;
    }
    
    public static final Situacao get(int codigo){
        switch(codigo){
            case 3: return ATIVO;
            default: return INATIVO;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dominio;

/**
 *
 * @author c8757550
 */
public enum Subsistema {
    CDSA(402), 
    CDSV(403),
    CFRNF(404),
    CISPOA(405),
    GERAL(406),
    REVENDA(407),
    FABRICANTE(408),
    PRODUTORRURAL(409),
    USUARIOEXTERNO(410);
    int cod;
    
    Subsistema(int cod){
        this.cod = cod;
    }
    
    public int getCod(){
        return this.cod;
    }
    
    public static final Subsistema get(int codigo){
        switch(codigo){
            case 402: return CDSA;
            case 403: return CDSV;
            case 404: return CFRNF;
            case 405: return CISPOA;
            case 406: return GERAL;
            case 407: return REVENDA;
            case 408: return FABRICANTE;
            case 409: return PRODUTORRURAL;
            case 410: return USUARIOEXTERNO;
            default: return null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.dominio;

/**
 *
 * @author c8757550
 */
public enum TipoPermissao {
    GATILHO(255), ACAO(256), PAPEL(257);
    int cod;

    TipoPermissao(int cod) {
        this.cod = cod;
    }
    
    public int getCod(){
        return this.cod;
    }
    
    public static final TipoPermissao get(int codigo){
        switch(codigo){
            case 255: return GATILHO;
            case 256: return ACAO;
            default: return PAPEL;
        }
    }
}

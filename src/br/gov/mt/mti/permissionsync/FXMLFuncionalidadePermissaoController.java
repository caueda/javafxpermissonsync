/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.FuncionalidadeDao;
import br.gov.mt.mti.permissionsync.dao.PermissaoDao;
import br.gov.mt.mti.permissionsync.interfaces.CallbackReturn;
import br.gov.mt.mti.permissionsync.vo.Funcionalidade;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author c8757550
 */
public class FXMLFuncionalidadePermissaoController extends AbstractController implements Initializable, CallbackReturn<Permissao> {

    @FXML
    private Button btnVincular;
    @FXML
    private Button btnSelecionarPermissao;
    @FXML
    private Button btnCancelar;
    @FXML
    private TableView<Permissao> tablePermissao;
    @FXML
    private TableColumn<Permissao,String> nome;
    @FXML
    private TableColumn<Permissao,String> descricao;
    @FXML
    private TableColumn<Permissao,String> action;
    @FXML
    private TableColumn<Permissao,Enum> tipoPermissao;
    @FXML
    private TableColumn<Permissao,Date> dataCriacao;    
    @FXML
    private TableColumn<Permissao,Date> dataAtualizacao;
    @FXML
    private TableColumn<Permissao,Integer> versao;
    
    private final ObservableList<Permissao> listaDePermissoes =
                FXCollections.observableArrayList();
    
    private Funcionalidade funcionalidadeSelecionada;
    
    private Permissao permissaoSelecionada;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        descricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));        
        action.setCellValueFactory(new PropertyValueFactory<>("action"));
        tipoPermissao.setCellValueFactory(new PropertyValueFactory<>("tipoPermissao"));        
        dataCriacao.setCellValueFactory(new PropertyValueFactory<>("dataCriacaoFormatada"));
        dataAtualizacao.setCellValueFactory(new PropertyValueFactory<>("dataAtualizacaoFormatada"));
        versao.setCellValueFactory(new PropertyValueFactory<>("versao"));           
                
        tablePermissao.setItems(listaDePermissoes);
        btnSelecionarPermissao.setOnAction(e -> {
            abrirFormVincularPermissao(this);
        });
        
        btnCancelar.setOnAction(e -> {
            close(e);
        });
        
        btnVincular.setOnAction(e -> {            
            FuncionalidadeDao funcionalidadeDao = null;
            try {
                funcionalidadeDao = new FuncionalidadeDao();
                Set<Permissao> gravar = new HashSet<>();
                Iterator<Permissao> ite = listaDePermissoes.iterator();
                while(ite.hasNext()){
                    gravar.add(ite.next());
                }                
                funcionalidadeDao.vincularPermissoes(getFuncionalidadeSelecionada().getId(), 
                        new ArrayList<Permissao>(gravar));
                showSucesso("Vinculação Realizada com Sucesso!");
                
                close(e);
            } catch(Exception e1){
                showException(e1);
            } finally {
                close(funcionalidadeDao);
            }
        });
        
        final ContextMenu tableMenu = new ContextMenu();
        MenuItem incluirItem = new MenuItem("Incluir Permissão");
        incluirItem.setOnAction(event -> {
                abrirFormVincularPermissao(this);
        });
        tableMenu.getItems().add(incluirItem);
        tablePermissao.setContextMenu(tableMenu);
        
        tablePermissao.setRowFactory(obj -> {            
            final ContextMenu rowMenu = new ContextMenu();            
            
            MenuItem removerItem = new MenuItem("Remover Permissão");            
            
            removerItem.setOnAction(event -> {
                this.listaDePermissoes.remove(getPermissaoSelecionada());
            });
            
            
            rowMenu.getItems().add(removerItem);
            
            TableRow<Permissao> row = new TableRow();
            row.contextMenuProperty().bind(
              Bindings.when(Bindings.isNotNull(row.itemProperty()))
              .then(rowMenu)
              .otherwise((ContextMenu)null));
            
            row.setOnMouseClicked(event ->{
                setPermissaoSelecionada(row.getItem());                
            });
            return row;
        });
    }

    @Override
    public Funcionalidade carregar(Long id) {
        if(id != null){    
            FuncionalidadeDao dao = null;
            try {
                dao = new FuncionalidadeDao();
                setPermissaoSelecionada(dao.load(id));            
            } catch(Exception e){
                showException(e);
            } finally {
                close(dao);
            }            
            setTitulo(getFuncionalidadeSelecionada().getNome());
            
            PermissaoDao permissaoDao = null;
            try {
                permissaoDao = new PermissaoDao();
                List<Permissao> listaPermissoesVinculadas = permissaoDao.listByFuncionalidadeId(id);
                this.listaDePermissoes.clear();
                this.listaDePermissoes.addAll(listaPermissoesVinculadas);
            } catch(Exception e){
                showException(e);
            } finally {
                close(permissaoDao);
            }
        }
        return getFuncionalidadeSelecionada();
    }

    public Funcionalidade getFuncionalidadeSelecionada() {
        return funcionalidadeSelecionada;
    }

    public void setPermissaoSelecionada(Funcionalidade funcionalidadeSelecionada) {
        this.funcionalidadeSelecionada = funcionalidadeSelecionada;
    }

    public Permissao getPermissaoSelecionada() {
        return permissaoSelecionada;
    }

    public void setPermissaoSelecionada(Permissao permissaoSelecionada) {
        this.permissaoSelecionada = permissaoSelecionada;
    }

    @Override
    public void callback(ObservableList<Permissao> object) {
        this.listaDePermissoes.addAll(object);        
    }
}

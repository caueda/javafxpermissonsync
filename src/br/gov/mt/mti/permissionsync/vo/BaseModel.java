/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.vo;

import br.gov.mt.mti.permissionsync.util.PropertyUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author c8757550
 */
public abstract class BaseModel implements Serializable {    
    private Long id;    
    private Date dataCriacao;
    private Date dataAtualizacao;
    private Integer versao;
    
    public Long getId(){
        return this.id;
    }
    
    public void setId(Long id){
        this.id = id;
    }    

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }
    
    public String getDataCriacaoFormatada(){
        if(getDataCriacao() != null)
            return PropertyUtil.getDataFormatada(getDataCriacao());
        else
            return "";
    }
    
    public String getDataAtualizacaoFormatada(){
        if(getDataAtualizacao() != null)
            return PropertyUtil.getDataFormatada(getDataAtualizacao());
        else
            return "";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseModel other = (BaseModel) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}

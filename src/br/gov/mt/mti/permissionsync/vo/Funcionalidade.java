/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.vo;

import br.gov.mt.mti.permissionsync.dominio.Situacao;
import br.gov.mt.mti.permissionsync.dominio.Subsistema;

/**
 *
 * @author c8757550
 */
public class Funcionalidade extends BaseModel {
    private String  nome;
    private String descricao;    
    private Situacao situacao = Situacao.ATIVO;
    private Subsistema subsistema = Subsistema.GERAL;
    private Long usrAtribuicaoFunc;
    
    public Funcionalidade(){
        super();
    }
    
    public Funcionalidade(String nome, String descricao, Situacao situacao, Subsistema subsistema){
        this.nome = nome;
        this.descricao = descricao;
        this.situacao = situacao;
        this.subsistema = subsistema;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Subsistema getSubsistema() {
        return subsistema;
    }

    public void setSubsistema(Subsistema subsistema) {
        this.subsistema = subsistema;
    }

    public Long getUsrAtribuicaoFunc() {
        return usrAtribuicaoFunc;
    }

    public void setUsrAtribuicaoFunc(Long usrAtribuicaoFunc) {
        this.usrAtribuicaoFunc = usrAtribuicaoFunc;
    }
}

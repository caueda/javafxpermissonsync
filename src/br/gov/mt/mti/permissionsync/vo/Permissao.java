/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.vo;

import br.gov.mt.mti.permissionsync.dominio.TipoPermissao;

/**
 *
 * @author c8757550
 */
public class Permissao extends BaseModel {
    //PERMISSAO_ID, TIPO_PERMISSAO, NOME, DESCRICAO, ACTION,
    private String  nome;
    private String descricao;    
    private String action;
    private TipoPermissao tipoPermissao = TipoPermissao.ACAO;
    
    public Permissao(){
        super();
    }
    
    public Permissao(String nome, String descricao, String action, TipoPermissao tipoPermissao){
        this.nome = nome;
        this.descricao = descricao;
        this.action = action;
        this.tipoPermissao = tipoPermissao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
    
    public TipoPermissao getTipoPermissao(){
        return tipoPermissao;
    }
    
    public void setTipoPermissao(TipoPermissao tipoPermissao){
        this.tipoPermissao = tipoPermissao;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.vo;

import br.gov.mt.mti.permissionsync.dominio.Situacao;

/**
 *
 * @author c8757550
 */
public class Perfil extends BaseModel {
    private String  nome;
    private String descricao;    
    private Situacao situacao = Situacao.ATIVO;
    
    public Perfil(){
        super();
    }
    
    public Perfil(String nome, String descricao, Situacao situacao){
        this.nome = nome;
        this.descricao = descricao;
        this.situacao = situacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.SincronizarDao;
import br.gov.mt.mti.permissionsync.dominio.Ambiente;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;

/**
 *
 * @author c8757550
 */
public class FXMLSincronizarController extends AbstractController<Permissao> implements Initializable {
  
    @FXML
    private CheckBox checkIntegra;
    @FXML
    private CheckBox checkHomologa;
    @FXML
    private CheckBox checkProducao;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        setTitulo("Sincronizar");
    }    
    
    private void clear(){
        
    }
    
    @FXML
    public void handleSubmitButtonAction(ActionEvent event){ 
        int i = 0;
          if(checkIntegra.isSelected()){
              i++;
              SincronizarDao dao = null;
              try {
                  dao = new SincronizarDao();
                  dao.executar(Ambiente.INTEGRACAO);
              } catch(Exception e){
                  showException(e);
              } finally {
                  close(dao);
              }
          }
          
          if(checkHomologa.isSelected()){
              i++;
              SincronizarDao dao = null;
              try {
                  dao = new SincronizarDao();
                  dao.executar(Ambiente.HOMOLOGACAO);
              } catch(Exception e){
                  showException(e);
              } finally {
                  close(dao);
              }
          }
          
          if(checkProducao.isSelected()){
              i++;
              SincronizarDao dao = null;
              try {
                  dao = new SincronizarDao();
                  dao.executar(Ambiente.PRODUCAO);
              } catch(Exception e){
                  showException(e);
              } finally {
                  close(dao);
              }
          }
          if(i==0){
              showInfo("Nenhuma banco de destino foi selecionado para a sincronização.");
          } else {
            showSucesso("Sincronização realizada com sucesso.");
          }
    }    

    @Override
    public Permissao carregar(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

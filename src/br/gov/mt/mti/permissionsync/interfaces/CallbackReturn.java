/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync.interfaces;

import javafx.collections.ObservableList;

/**
 *
 * @author c8757550
 */
public interface CallbackReturn<T> {
    public void callback(ObservableList<T> observable);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.PermissaoDao;
import br.gov.mt.mti.permissionsync.dao.exception.ObjectAlreadyExistsException;
import br.gov.mt.mti.permissionsync.dominio.TipoPermissao;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 *
 * @author c8757550
 */
public class FXMLPermissionController extends AbstractController<Permissao> implements Initializable {
  
    @FXML
    private TextField nome;
    @FXML
    private TextField action;
    @FXML
    private TextField descricao;
    @FXML
    private Button btnGravar;
    @FXML
    private ComboBox<TipoPermissao> tipo;
    private Permissao permissao;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        tipo.getItems().addAll(TipoPermissao.GATILHO, TipoPermissao.ACAO, TipoPermissao.PAPEL);
        tipo.setEditable(false);
        setTitulo("Permissão");
    }    
    
    private void clear(){
        nome.setText(null);
        action.setText(null);
        descricao.setText(null);
        tipo.setValue(null);
    }
    
    @FXML
    public void handleSubmitButtonAction(ActionEvent event){        
        
        if(nome.getText() == null || nome.getText().isEmpty()){
            showInfo("Por favor informe o \"Nome\".");
            return;
        } else if(action.getText() == null || action.getText().isEmpty()){
            showInfo("Por favor informe a \"Ação\".");
            return;
        } else if(descricao.getText() == null || descricao.getText().isEmpty()){
            showInfo("Por favor informe a \"Descrição\".");
            return;
        } else if(tipo == null || tipo.getValue() == null){
            showInfo("Por favor informe o \"Tipo de Permissão\".");
            return;
        }
        
        getPermissao().setNome(nome.getText());
        getPermissao().setDescricao(descricao.getText());
        getPermissao().setAction(action.getText());
        getPermissao().setTipoPermissao(tipo.getValue());
        
        PermissaoDao dao = null;
        
        try {
            dao = new PermissaoDao();
            if(getPermissao().getId() == null){
                dao.persist(getPermissao());
            } else {
                dao.update(getPermissao());
            }
            showSucesso("Registro gravado.");    
        } catch(ObjectAlreadyExistsException e){
            showInfo(e.getMessage());
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }        
        
        clear();
        if(getPermissao().getId() != null){
         close(event);    
        }        
    }

    @Override
    public Permissao carregar(Long id) {
        if(id != null){            
            btnGravar.setText("Alterar");

            PermissaoDao dao = null;
            try {
                dao = new PermissaoDao();
                setPermissao(dao.load(id));               
            } catch(Exception e){
                showException(e);
            } finally {
                close(dao);
            }
            
            nome.setText(getPermissao().getNome());
            action.setText(getPermissao().getAction());
            descricao.setText(getPermissao().getDescricao());
            tipo.setValue(getPermissao().getTipoPermissao());            
        }
        return getPermissao();
    }

    public Permissao getPermissao() {
        if(this.permissao == null){
            setPermissao(new Permissao());
        }
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.PermissaoDao;
import br.gov.mt.mti.permissionsync.dominio.TipoPermissao;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.net.URL;
import java.sql.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author c8757550
 */
public class FXMLPesquisaPermissaoController extends AbstractController implements Initializable {

    @FXML
    private TextField pesquisaAcao;
    @FXML
    private ComboBox<TipoPermissao> pesquisaComboSituacao;
    @FXML
    private TableView<Permissao> tablePermissao;
    @FXML
    private TableColumn<Permissao,String> nome;
    @FXML
    private TableColumn<Permissao,String> descricao;
    @FXML
    private TableColumn<Permissao,String> action;
    @FXML
    private TableColumn<Permissao,Enum> tipoPermissao;
    @FXML
    private TableColumn<Permissao,Date> dataCriacao;    
    @FXML
    private TableColumn<Permissao,Date> dataAtualizacao;
    @FXML
    private TableColumn<Permissao,Integer> versao;
    
    
    private final ObservableList<Permissao> listaDePermissoes =
                FXCollections.observableArrayList();
    
    private Permissao permissaoSelecionada;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        descricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));        
        action.setCellValueFactory(new PropertyValueFactory<>("action"));
        tipoPermissao.setCellValueFactory(new PropertyValueFactory<>("tipoPermissao"));        
        dataCriacao.setCellValueFactory(new PropertyValueFactory<>("dataCriacaoFormatada"));
        dataAtualizacao.setCellValueFactory(new PropertyValueFactory<>("dataAtualizacaoFormatada"));
        versao.setCellValueFactory(new PropertyValueFactory<>("versao"));
        
        
        pesquisaComboSituacao.getItems().addAll(TipoPermissao.GATILHO, TipoPermissao.ACAO, TipoPermissao.PAPEL);
        pesquisaComboSituacao.setEditable(false);  
        
        setTitulo("Pesquisar Permissão");
        
        PermissaoDao dao = null;
        
        try {
            dao = new PermissaoDao();
            listaDePermissoes.addAll(dao.top10());
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }
        
        tablePermissao.setItems(listaDePermissoes);
        
        tablePermissao.setRowFactory(obj -> {
            final ContextMenu rowMenu = new ContextMenu();
            MenuItem editItem = new MenuItem("Editar");            
            MenuItem excluirItem = new MenuItem("Excluir");            
            
            editItem.setOnAction(event -> {
                abrirFormPermissao(getPermissaoSelecionada().getId());
            });            
            excluirItem.setOnAction(event -> {                
                excluir(getPermissaoSelecionada());
            });
            
            rowMenu.getItems().add(editItem);
            rowMenu.getItems().add(excluirItem);
            
            TableRow<Permissao> row = new TableRow();
            row.contextMenuProperty().bind(
              Bindings.when(Bindings.isNotNull(row.itemProperty()))
              .then(rowMenu)
              .otherwise((ContextMenu)null));
            
            row.setOnMouseClicked(event ->{
                setPermissaoSelecionada(row.getItem());
                if(event.getClickCount() == 2 && !row.isEmpty()){                    
                    abrirFormPermissao(row.getItem().getId());
                }
            });
            return row;
        });
    }

    @FXML
    public void pesquisar(ActionEvent event){
        Permissao pesquisar = new Permissao();
        pesquisar.setAction(pesquisaAcao.getText());
        pesquisar.setTipoPermissao(pesquisaComboSituacao.getValue());
        listaDePermissoes.clear();
        PermissaoDao dao = null;
        try {
            dao = new PermissaoDao();
            listaDePermissoes.addAll(dao.list(pesquisar));   
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }        
    }
    
    @FXML
    public void limparForm(ActionEvent event){
        pesquisaAcao.setText(null);
        pesquisaComboSituacao.setValue(null);
        listaDePermissoes.clear();
    }
    
    public void excluir(Permissao permissao){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmação");
        alert.setHeaderText("Deseja realmente excluir a Permissão ?");
        alert.setContentText("Você estará excluindo a permissão do sistema.\n"
                + "Deseja continuar com a exclusão?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            PermissaoDao dao = null;
            try {
                dao = new PermissaoDao();
                dao.excluir(permissao);
                this.listaDePermissoes.remove(permissao);
                showSucesso("Permissão excluída com sucesso!");
            } catch(Exception e){
            } finally {
                close(dao);
            }
        }
    }

    @Override
    public Permissao carregar(Long id) {
        if(id != null){    
            PermissaoDao dao = null;
            try {
                dao = new PermissaoDao();
                setPermissaoSelecionada(dao.load(id));            
            } catch(Exception e){
                showException(e);
            } finally {
                close(dao);
            }            
            nome.setText(getPermissaoSelecionada().getNome());
            action.setText(getPermissaoSelecionada().getAction());
            descricao.setText(getPermissaoSelecionada().getDescricao());            
        }
        return getPermissaoSelecionada();
    }

    public Permissao getPermissaoSelecionada() {
        return permissaoSelecionada;
    }

    public void setPermissaoSelecionada(Permissao permissaoSelecionada) {
        this.permissaoSelecionada = permissaoSelecionada;
    }    
}

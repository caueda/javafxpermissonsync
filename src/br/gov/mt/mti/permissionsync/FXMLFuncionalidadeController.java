/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.FuncionalidadeDao;
import br.gov.mt.mti.permissionsync.dao.exception.ObjectAlreadyExistsException;
import br.gov.mt.mti.permissionsync.dominio.Situacao;
import br.gov.mt.mti.permissionsync.dominio.Subsistema;
import br.gov.mt.mti.permissionsync.vo.Funcionalidade;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 *
 * @author c8757550
 */
public class FXMLFuncionalidadeController extends AbstractController implements Initializable {
  
    @FXML
    private TextField nome;
    @FXML
    private TextField descricao;
    @FXML
    private ComboBox<Subsistema> subsistema;
    @FXML
    private ComboBox<Situacao> situacao;
    @FXML
    private Button btnGravar;    
    
    private Long id;
    private Funcionalidade funcionalidade;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        situacao.getItems().addAll(Situacao.ATIVO, Situacao.INATIVO);
        situacao.setEditable(false);
        
        subsistema.getItems().addAll(Arrays.asList(Subsistema.values()));
        subsistema.setEditable(false);
        
        setTitulo("Funcionalidade");
    }    
    
    private void clear(){
        nome.setText(null);
        descricao.setText(null);
        subsistema.setValue(null);
        situacao.setValue(null);
    }
    
    @FXML
    public void handleSubmitButtonAction(ActionEvent event){
        if(nome.getText() == null || nome.getText().isEmpty()){
            showInfo("Por favor informe o \"Nome\".");
            return;
        } else if(descricao.getText() == null || descricao.getText().isEmpty()){
            showInfo("Por favor informe a \"Descrição\".");
            return;
        } else if(situacao == null || situacao.getValue() == null){
            showInfo("Por favor informe o \"Situação\".");
            return;
        }
        
        getFuncionalidade().setNome(nome.getText());
        getFuncionalidade().setDescricao(descricao.getText());
        getFuncionalidade().setSubsistema(subsistema.getValue());
        getFuncionalidade().setSituacao(situacao.getValue());
        
        FuncionalidadeDao dao = null;
        
        try {
            dao = new FuncionalidadeDao();
            if(getFuncionalidade().getId() == null){
                dao.persist(funcionalidade);
            } else {
                dao.update(funcionalidade);
            }
            showSucesso("Registro gravado.");
            if(getFuncionalidade().getId() != null){
                cancelar(event);
            }
        } catch(ObjectAlreadyExistsException e){
            showInfo(e.getMessage());
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }
        
        clear();
        //close(event);
    }
    
    @Override
    public Funcionalidade carregar(Long id) {        
        if(id != null){
            this.id = id;
            btnGravar.setText("Alterar");
            
            FuncionalidadeDao dao = null;
            
            try {
                dao = new FuncionalidadeDao();
                setFuncionalidade(dao.load(id));
            } catch(Exception e){
                showException(e);
            } finally {
                close(dao);
            }
            
            nome.setText(getFuncionalidade().getNome());
            descricao.setText(getFuncionalidade().getDescricao());    
            situacao.setValue(getFuncionalidade().getSituacao());
            subsistema.setValue(getFuncionalidade().getSubsistema());
        }
        return getFuncionalidade();
    }

    public Funcionalidade getFuncionalidade() {
        if(funcionalidade == null){
            setFuncionalidade(new Funcionalidade());
        }
        return funcionalidade;
    }

    public void setFuncionalidade(Funcionalidade funcionalidade) {
        this.funcionalidade = funcionalidade;
    }
}

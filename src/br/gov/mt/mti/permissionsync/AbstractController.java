/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.AbstractDao;
import br.gov.mt.mti.permissionsync.help.FXMLSobreController;
import br.gov.mt.mti.permissionsync.interfaces.CallbackReturn;
import br.gov.mt.mti.permissionsync.vo.BaseModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author c8757550
 */
public abstract class AbstractController<T extends BaseModel> {

    @FXML
    protected BorderPane mainBorderPane;
    
    private CallbackReturn callbackReturn;

    public AbstractController() {
        super();
    }

    public abstract T carregar(Long id);

    public void showInfo(String header, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alerta");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public void showInfo(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alerta");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public void showSucesso(String header, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alerta");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public void showSucesso(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alerta");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public void showException(Exception ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Janela de Exceção");
        alert.setHeaderText("Alguma coisa \"deu ruim\" !");
        if(ex.getCause() != null){
            alert.setContentText(ex.getCause().getMessage());        
        } else
            alert.setContentText(ex.getMessage());        

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("A pilha de exceções foi:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public void close(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    @FXML
    public void cancelar(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    public void abrirFormFuncionalidade(Long id) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLFuncionalidade.fxml"));
            Parent root = loader.load();
            FXMLFuncionalidadeController controller = loader.<FXMLFuncionalidadeController>getController();
            controller.carregar(id);
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLFuncionalidadeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void abrirFormPermissao(Long id) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLPermission.fxml"));
            root = loader.load();
            FXMLPermissionController controller = loader.<FXMLPermissionController>getController();
            controller.carregar(id);
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLFuncionalidadeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void abrirFormPermissaoPesquisar() {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLPesquisaPermissao.fxml"));
            root = loader.load();
            FXMLPesquisaPermissaoController controller = loader.<FXMLPesquisaPermissaoController>getController();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLFuncionalidadeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void abrirFormFuncionalidadePermissao(Long id) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLFuncionalidadePermissao.fxml"));
            root = loader.load();
            FXMLFuncionalidadePermissaoController controller = loader.<FXMLFuncionalidadePermissaoController>getController();
            controller.carregar(id);
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLFuncionalidadeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void abrirFormVincularPermissao(CallbackReturn callbackReturn) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLVincularPermissao.fxml"));
            root = loader.load();
            FXMLVincularPermissaoController controller = loader.<FXMLVincularPermissaoController>getController();
            controller.setCallbackReturn(callbackReturn);
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLFuncionalidadeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void abrirFormSincronizar(CallbackReturn callbackReturn) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLSincronizar.fxml"));
            root = loader.load();
            FXMLSincronizarController controller = loader.<FXMLSincronizarController>getController();
            controller.setCallbackReturn(callbackReturn);
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLFuncionalidadeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void abrirFormSobre() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("help/FXMLSobre.fxml"));

            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLSobreController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void close(AbstractDao dao) {
        if (dao != null) {
            dao.close();
        }
    }

    @FXML
    public void sair(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    public CallbackReturn getCallbackReturn() {
        return callbackReturn;
    }

    public void setCallbackReturn(CallbackReturn callback) {
        this.callbackReturn = callback;
    }
    
    private FlowPane getTopBanner(String titulo){
        FlowPane topBanner = new FlowPane();
        topBanner.setPrefHeight(40);
        topBanner.setVgap(4);
        topBanner.setHgap(6);
        String backgroundStyle = 
                "-fx-background-color: lightblue;"
                + "-fx-background-radius:0%;"
                + "-fx-background-inset: 5px;"
                + "-fx-padding: 8;";
        topBanner.setStyle(backgroundStyle);
        SVGPath svgIcon = new SVGPath();        
        
        svgIcon.setContent("M18.208,2.958H8.875V1.792c0-0.644-0.522-1.167-1.167-1.167H1.875c-0.644,0-1.167,0.522-1.167,1.167v16.333\n" +
"								c0,0.645,0.522,1.166,1.167,1.166h16.333c0.645,0,1.167-0.521,1.167-1.166v-14C19.375,3.48,18.853,2.958,18.208,2.958z\n" +
"								 M18.208,17.542c0,0.322-0.261,0.583-0.583,0.583H2.458c-0.323,0-0.583-0.261-0.583-0.583V6.458h16.333V17.542z M17.625,5.292\n" +
"								H1.875V2.375c0-0.323,0.261-0.583,0.583-0.583h4.667c0.323,0,0.583,0.261,0.583,0.583v1.75h9.917c0.322,0,0.583,0.261,0.583,0.583\n" +
"								C18.208,5.031,17.947,5.292,17.625,5.292z");
        svgIcon.setStroke(Color.WHITE);
        svgIcon.setFill(Color.WHITE);        
        Text tituloBanner = new Text(titulo);
        tituloBanner.setFill(Color.WHITE);
        Font serif = Font.font("Dialog", 30);
        tituloBanner.setFont(serif);
        topBanner.getChildren().addAll(svgIcon, tituloBanner);
        
        return topBanner;
    }
    
    public void setTitulo(String titulo){
        this.mainBorderPane.setTop(getTopBanner(titulo));
    }
}

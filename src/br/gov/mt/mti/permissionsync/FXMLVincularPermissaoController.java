/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.mt.mti.permissionsync;

import br.gov.mt.mti.permissionsync.dao.PermissaoDao;
import br.gov.mt.mti.permissionsync.dominio.TipoPermissao;
import br.gov.mt.mti.permissionsync.vo.Permissao;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.util.Callback;

/**
 *
 * @author c8757550
 */
public class FXMLVincularPermissaoController extends AbstractController implements Initializable {

    @FXML
    private TextArea pesquisaAcao;
    @FXML
    private Button btnIncluir;     
    @FXML
    private Button btnFechar;
    @FXML
    private ComboBox<TipoPermissao> pesquisaComboSituacao;
    @FXML
    private ListView listView;
    
    private final ObservableList<Permissao> listaDePermissoes =
                FXCollections.observableArrayList();
    private final ObservableList<Permissao> listaDePermissoesSelecionadas =
                FXCollections.observableArrayList();
    
    private Permissao permissaoSelecionada;
    
    private void incluirPermissao(){
        if(this.listaDePermissoesSelecionadas.isEmpty()){
                showInfo("Por favor, selecione pelo menos uma permissão.");
                return;
        }
        getCallbackReturn().callback(listaDePermissoesSelecionadas);
        this.listaDePermissoes.removeAll(listaDePermissoesSelecionadas);
        //close(event);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        setTitulo("Vincular Permissões");
        pesquisaComboSituacao.getItems().addAll(TipoPermissao.GATILHO, TipoPermissao.ACAO, TipoPermissao.PAPEL);
        pesquisaComboSituacao.setEditable(false);        
        
        btnIncluir.setOnAction(event -> {
            incluirPermissao();
        });
        
        btnFechar.setOnAction(t -> {
            close(t);
        });
        
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setCellFactory(new Callback<ListView<Permissao>, ListCell<Permissao>>() {
            @Override
            public ListCell<Permissao> call(ListView<Permissao> param) {
                ListCell<Permissao> cell = new ListCell<Permissao>() {

                    @Override
                    protected void updateItem(Permissao item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                           setText(item.getAction());
                        }
                    }
                };
                return cell;
            }
        });
        
        listView.setOnMouseClicked(e -> {
            this.listaDePermissoesSelecionadas.clear(); 
            this.listaDePermissoesSelecionadas.addAll(listView.getSelectionModel().getSelectedItems());
            
            if(e.getClickCount() == 2){
                 incluirPermissao();
             }
            
        });        
        
        listView.setItems(listaDePermissoes);      
    }

    @FXML
    public void pesquisar(ActionEvent event){
        Permissao pesquisar = new Permissao();
        pesquisar.setAction(pesquisaAcao.getText());
        pesquisar.setTipoPermissao(pesquisaComboSituacao.getValue());
        listaDePermissoes.clear();
        listaDePermissoesSelecionadas.clear();
        PermissaoDao dao = null;
        try {
            dao = new PermissaoDao();
            listaDePermissoes.addAll(dao.list(pesquisar));    
        } catch(Exception e){
            showException(e);
        } finally {
            close(dao);
        }
    }
    
    @FXML
    public void limparForm(ActionEvent event){
        pesquisaAcao.setText(null);
        pesquisaComboSituacao.setValue(null);
        listaDePermissoes.clear();
        listaDePermissoesSelecionadas.clear();
    }

    @Override
    public Permissao carregar(Long id) {
        if(id != null){    
            PermissaoDao dao = null;
            try {
                dao = new PermissaoDao();
                setPermissaoSelecionada(dao.load(id));            
            } catch(Exception e){
                showException(e);
            } finally {
                close(dao);
            }
        }
        return getPermissaoSelecionada();
    }

    public Permissao getPermissaoSelecionada() {
        return permissaoSelecionada;
    }

    public void setPermissaoSelecionada(Permissao permissaoSelecionada) {
        this.permissaoSelecionada = permissaoSelecionada;
    }    
}
